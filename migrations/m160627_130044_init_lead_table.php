<?php

use yii\db\Migration;

class m160627_130044_init_lead_table extends Migration
{
    public function up()
    {
		$this -> createTable(
	 'lead',[
	 'id' => 'pk',
	 'name'=> 'string',
	 'birthDate' => 'date',
	 'status' => 'int',
	 'notes' => 'text',
 ],
 'ENGINE =InnoDB'
 );
    }

    public function down()
    {
        echo "m160627_130044_init_lead_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
